FROM openjdk:8-jdk-alpine
COPY target/autopaleis-rest-service-0.0.1.jar auto-paleis-rest-service-0.0.1.jar
ENTRYPOINT ["java","-jar","/auto-paleis-rest-service-0.0.1.jar"]