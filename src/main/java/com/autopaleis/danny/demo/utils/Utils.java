package com.autopaleis.danny.demo.utils;

import java.util.UUID;

public class Utils {
    
    public static String generateId() {
        return UUID.randomUUID().toString().replace("-", "").substring(0, 24);
    }
}
