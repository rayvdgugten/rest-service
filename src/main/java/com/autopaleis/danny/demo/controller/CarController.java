package com.autopaleis.danny.demo.controller;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicInteger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.autopaleis.danny.demo.model.Car;
import com.autopaleis.danny.demo.services.CarServices;
import com.autopaleis.danny.demo.utils.Utils;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/api/car")
public class CarController {
    @Autowired
    private CarServices carServices = new CarServices();

    @PostMapping()
    public Car createCar(@RequestBody Car car) {
        System.out.println(car.isAvailable());
        car.set_id(Utils.generateId().toString());
        carServices.postCar(car);
        
        return car;
    }

    @PutMapping("/{id}")
    public Car updateCar(@RequestBody Car car, @PathVariable String id) {
        return carServices.putCar(car, id);
    }

    @GetMapping("/{id}")
    public Optional<Car> getCarById(@PathVariable String id) {

       return carServices.getCarById(id);
    }

    @GetMapping("/getCarsByAvailability")
    public List<Car> getCarByManufacturer(@RequestParam(value="available", defaultValue="true") boolean available) {

       return carServices.getCarsByAvailability(available);
    }

    @GetMapping()
    public List<Car> getAll() {
        return carServices.getAll();
    }
    
}
