package com.autopaleis.danny.demo.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.autopaleis.danny.demo.model.Car;
import com.autopaleis.danny.demo.repository.ItemRepository;

@Service
public class CarServices {

    @Autowired
    ItemRepository carItemRepository;

    public void postCar(Car Car) {
        System.out.println("Inserting Car into database...");
        carItemRepository.save(Car);
        System.out.println("Insertion complete!");
    };

    public Optional<Car> getCarById(String id) {
        return carItemRepository.findById(id);
    }

    public List<Car> getCarsByAvailability(boolean available) {
        return carItemRepository.findAll(available);
    }

    public List<Car> getAll() {
        return carItemRepository.findAll();
    }

    public Car putCar(Car newCar, String id) {
        return carItemRepository.findById(id).map(car -> {
            car.setName(newCar.getName());
            car.setAvailable(newCar.isAvailable());
            return carItemRepository.save(car);
        }).orElseGet(() -> {
            return carItemRepository.save(newCar);
        });
    }

}
