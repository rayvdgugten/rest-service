package com.autopaleis.danny.demo.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import com.autopaleis.danny.demo.model.Car;

public interface ItemRepository extends MongoRepository<Car, String> {
    @Query("{name:'?0'}")
    Car findItemByName(String name);

    @Query(value="{available:'?0'}")
    List<Car> findAll(Boolean available);

    public long count();
}
