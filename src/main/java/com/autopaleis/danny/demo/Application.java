package com.autopaleis.danny.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

@SpringBootApplication
@EnableMongoRepositories
public class Application {

	private static Logger logger = LogManager.getLogger();

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
		logger.info("It is a info logger.");
	}

}
