package com.autopaleis.danny.demo.security;

import java.util.ArrayList;
import java.util.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;

@Configuration
public class CorsConfig {
 
  @Bean
  public CorsFilter corsFilter() {
    UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
    CorsConfiguration corsConfiguration = new CorsConfiguration();
    List<String> allowedOrigins = new ArrayList<String>();
    List<String> configuList = new ArrayList<String>();
    allowedOrigins.add("http://localhost:3000/");
    configuList.add(CorsConfiguration.ALL);
 
    corsConfiguration.setAllowCredentials(true);
    corsConfiguration.setAllowedOrigins(allowedOrigins);
    corsConfiguration.setAllowedMethods(configuList);
    corsConfiguration.setAllowedHeaders(configuList);
    source.registerCorsConfiguration("/**", corsConfiguration);
    return new CorsFilter(source);
  }
}
